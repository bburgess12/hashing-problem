/*************************************************************************
 * Compilation:  javac Degrees.java
 * Execution:    java Degrees filename.txt
 * Dependencies: Digraph.java Stack.java In.java StdOut.java
 * <p>
 * % java Digraph GraphCyclesMap.txt
 * Indegrees from a vertex 0 is 1
 * Outdegrees from a vertex 0 is 1
 * Indegrees from a vertex 1 is 1
 * Outdegrees from a vertex 1 is 1
 * Indegrees from a vertex 2 is 1
 * Outdegrees from a vertex 2 is 1
 * Indegrees from a vertex 3 is 1
 * Outdegrees from a vertex 3 is 1
 * The list of sources has nothing!
 * The list of sinks has nothing!
 * The digraph is a map
 *************************************************************************/

/**
 * Based on a Digraph, Degrees.java computes the number of indegrees and outdegrees
 * for every vertex, the list of sources and sinks and check whether a digraph is a map
 */
public class Degrees {
    //instance vars
    private Digraph digraph;
    private int[] inDegree;                 //store the number of incoming edges for every vertex
    private int[] outDegree;                //store the number of out-going edges for every vertex
    private Stack<Integer> sources;         //stack of the sources
    private Stack<Integer> sinks;           //stack of the sinks
    private boolean isMap;                  //check if a digraph is a map

    /**
     * Construct the Degrees object,
     * compute the number of incoming and out-going edges, the sources, the sinks
     *
     * @param G is a digraph
     */
    public Degrees(Digraph G) {
        digraph = new Digraph( G );
        sources = new Stack<>();
        sinks = new Stack<>();
        inDegree = new int[digraph.V()];
        outDegree = new int[digraph.V()];
        Digraph reversedGraph = digraph.reverse(); //rebuild the reverse digraph
        isMap = true;

        for (int v = 0; v < digraph.V(); v++) {
            inDegree[v] = reversedGraph.outdegree( v );
            outDegree[v] = digraph.outdegree( v );

            if (inDegree[v] == 0) {
                sources.push( v );
            }

            if (outDegree[v] == 0) {
                sinks.push( v );
            }
            //check whether a digraph is a map
            if (digraph.outdegree( v ) != 1) {
                isMap = false;
            }
        }
    }

    /**
     * Computes the number of incoming edges
     *
     * @return the number of incoming edges
     */
    public int indegree(int v) {
        return inDegree[v];
    }

    /**
     * Computes the number of outgoing edges
     *
     * @return the number of outgoing edges
     */
    public int outdegree(int v) {
        return outDegree[v];
    }

    /**
     * Computes the number of sources
     *
     * @return a stack of sources
     */
    public Iterable<Integer> sources() {
        return sources;
    }

    /**
     * Computes the number of sinks
     *
     * @return a stack of sinks
     */
    public Iterable<Integer> sinks() {
        return sinks;
    }

    /**
     * Check whether a digraph is a map
     *
     * @return true if it's a map, otherwise false
     */
    public boolean isMap() {
        return isMap;
    }

    //demo client tests for the functionality of Degrees based on Digraph with a given input file
    public static void main(String[] args) {
        In in = new In( args[0] );
        Digraph digraph = new Digraph( in );
        Degrees degrees = new Degrees( digraph );

        //computes the number of indegrees and outdegrees
        for (int v = 0; v < digraph.V(); v++) {
            StdOut.println( "Indegrees from a vertex " + v + " is " + degrees.indegree( v ) );
            StdOut.println( "Outdegrees from a vertex " + v + " is " + degrees.outdegree( v ) );
        }

        //the list of sources
        StdOut.print( "The list of sources " );
        int numberOfSources = 0;
        for (int source : degrees.sources()) {
            numberOfSources++;
            StdOut.print( source );
        }
        if (numberOfSources <= 0)
            StdOut.println( "has nothing!" );

        //the list of sinks
        StdOut.print( "The list of sinks " );
        int numberOfSinks = 0;
        for (int sink : degrees.sinks()) {
            numberOfSinks++;
            StdOut.print( sink );
        }
        if (numberOfSinks <= 0)
            StdOut.println( "has nothing!" );

        if (degrees.isMap())
            StdOut.println( "The digraph is a map" );
        else
            StdOut.println( "The digraph is not a map" );

    }
}

