import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by Brendan Burgess & Chung Nguyen on 11/1/2015.
 */
public class DegreesTester {

    //private Digraph emptyGraph = new Digraph(new In("EmptyGraph.txt"));
    private Digraph graphAllSinks = new Digraph(new In("GraphAllSinks.txt"));
    private Digraph graphAllSource = new Digraph(new In("GraphAllSource.txt"));
    private Digraph graphCyclesMap = new Digraph(new In("GraphCyclesMap.txt"));
    private Digraph graphLinear = new Digraph(new In("GraphLinear.txt"));
    private Digraph graphMapSelfLoop = new Digraph(new In("GraphMapSelfLoop.txt"));
    private Digraph graphMultipleSources = new Digraph(new In("GraphMultipleSources.txt"));
    private Digraph graphNoSources = new Digraph(new In("GraphNoSources.txt"));
    private Digraph graphTwoSelfLoops = new Digraph(new In("GraphTwoSelfLoops.txt"));

    private Degrees emptyGraphDegrees;
    private Degrees graphAllSinksDegrees;
    private Degrees graphAllSourceDegrees;
    private Degrees graphCyclesMapDegrees;
    private Degrees graphLinearDegrees;
    private Degrees graphMapSelfLoopDegrees;
    private Degrees graphMultipleSourcesDegrees;
    private Degrees graphNoSourcesDegrees;
    private Degrees graphTwoSelfLoopsDegrees;


    @Before
    public void setUp() throws Exception {
        // emptyGraphDegrees = new Degrees(emptyGraph);
        graphAllSinksDegrees = new Degrees(graphAllSinks);
        graphAllSourceDegrees = new Degrees(graphAllSource);
        graphCyclesMapDegrees = new Degrees(graphCyclesMap);
        graphLinearDegrees = new Degrees(graphLinear);
        graphMapSelfLoopDegrees = new Degrees(graphMapSelfLoop);
        graphMultipleSourcesDegrees = new Degrees(graphMultipleSources);
        graphNoSourcesDegrees = new Degrees(graphNoSources);
        graphTwoSelfLoopsDegrees = new Degrees(graphTwoSelfLoops);
    }

    /*****************************************************************************************************************
     * Testing for inDegrees and outDegrees
     *****************************************************************************************************************/
	@Test
	public void indegreesOneSourceAllSinks() {
		// Digraph with all sinks, one source
		for (int vertex = 1; vertex < graphAllSinks.V(); vertex++) {
			assertEquals(graphAllSinksDegrees.indegree(vertex), 1);
		}
		assertEquals(graphAllSinksDegrees.indegree(0), 0);
	}

	@Test
	public void indegreesOneSinkAllSources() {
		// Digraph with all sources, one sink
		for (int vertex = 0; vertex < graphAllSource.E(); vertex++) {
			assertEquals(graphAllSourceDegrees.indegree(vertex), 0);
		}
		assertEquals(graphAllSourceDegrees.indegree(3), 3);
	}

	@Test
	public void indegreesCycleGraph() {
		// Digraph with a cycle and is a map
		for (int vertex = 0; vertex < graphCyclesMap.V(); vertex++) {
			assertEquals(graphCyclesMapDegrees.indegree(vertex), 1);
		}
	}

	@Test
	public void indegreesMapSelfLoop() {
		// Digraph which has a self loop and is a map
		for (int vertex = 1; vertex < graphMapSelfLoop.V() - 1; vertex++) {
			assertEquals(graphMapSelfLoopDegrees.indegree(vertex), 1);
		}
		assertEquals(graphMapSelfLoopDegrees.indegree(0), 0);
		assertEquals(graphMapSelfLoopDegrees.indegree(3), 2);
	}

	@Test
	public void indegreesNoSources() {
		// Digraph with no sources
		for (int vertex = 0; vertex < graphNoSources.V(); vertex++) {
			assertEquals(graphNoSourcesDegrees.indegree(vertex), 1);
		}
	}

	@Test
	public void indegreesMultipleSources() {
		// Digraph with multiple sources
		for (int vertex = 1; vertex < graphMultipleSources.V() - 1; vertex++) {
			assertEquals(graphMultipleSourcesDegrees.indegree(vertex), 2);
		}
		assertEquals(graphMultipleSourcesDegrees.indegree(0), 0);
		assertEquals(graphMultipleSourcesDegrees.indegree(4), 0);
	}

	@Test
	public void indegreesTwoSelfLoops() {

		// Digraph with two self loops
		for (int vertex = 0; vertex < graphTwoSelfLoops.V() - 1; vertex++) {
			assertEquals(graphTwoSelfLoopsDegrees.indegree(vertex), 1);
		}
		assertEquals(graphTwoSelfLoopsDegrees.indegree(2), 2);
	}

	@Test
	public void outdegreesOneSourceAllSinks() {
		// Digraph with all sinks, one source
		for (int vertex = 1; vertex < graphAllSinks.V(); vertex++) {
			assertEquals(graphAllSinksDegrees.outdegree(vertex), 0);
		}
		assertEquals(graphAllSinksDegrees.outdegree(0), 3);
	}

	@Test
	public void outdegreesOneSinkAllSources() {
		// Digraph with all sources, one sink
		for (int vertex = 0; vertex < graphAllSource.E(); vertex++) {
			assertEquals(graphAllSourceDegrees.outdegree(vertex), 1);
		}
		assertEquals(graphAllSourceDegrees.outdegree(3), 0);
	}

	@Test
	public void outdegreesCycleGraph() {
		// Digraph with a cycle and is a map
		for (int vertex = 0; vertex < graphCyclesMap.V(); vertex++) {
			assertEquals(graphCyclesMapDegrees.outdegree(vertex), 1);
		}
	}

	@Test
	public void outdegreesMapSelfLoop() {
		// Digraph which has a self loop and is a map
		for (int vertex = 0; vertex < graphMapSelfLoop.V(); vertex++) {
			assertEquals(graphMapSelfLoopDegrees.outdegree(vertex), 1);
		}
	}

	@Test
	public void outdegreesNoSources() {
		// Digraph with no sources
		assertEquals(graphNoSourcesDegrees.outdegree(0), 2);
		assertEquals(graphNoSourcesDegrees.outdegree(1), 1);
		assertEquals(graphNoSourcesDegrees.outdegree(2), 0);
	}

	@Test
	public void outdegreesMultipleSources() {
		// Digraph with multiple sources
		for (int vertex = 1; vertex < graphMultipleSources.V() - 1; vertex++) {
			assertEquals(graphMultipleSourcesDegrees.outdegree(vertex), 0);
		}
		assertEquals(graphMultipleSourcesDegrees.outdegree(0), 3);
		assertEquals(graphMultipleSourcesDegrees.outdegree(4), 3);
	}

	@Test
	public void outdegreesTwoSelfLoops() {
		// Digraph with two self loops
		for (int vertex = 1; vertex < graphTwoSelfLoops.V(); vertex++) {
			assertEquals(graphTwoSelfLoopsDegrees.outdegree(vertex), 1);
		}
		assertEquals(graphTwoSelfLoopsDegrees.outdegree(0), 2);
	}

    /*****************************************************************************************************************
     * Testing for sinks and sources
     *****************************************************************************************************************/
    public SeparateChainingHashST<Integer, Integer> hashTableBuild(Iterable<Integer> tempStorage) {
        SeparateChainingHashST<Integer, Integer> sources = new SeparateChainingHashST<>();
        for (Integer i : tempStorage) {
            sources.put(i, 1);
        }
        return sources;
    }

    @Test
    public void testAllSinksSources() {
        SeparateChainingHashST<Integer, Integer> allSinksSources = hashTableBuild(graphAllSinksDegrees.sources());
        assertEquals(1, allSinksSources.size());
        assertEquals(true, allSinksSources.contains(0));
    }

    @Test
    public void testAllSinksSinks() {
        SeparateChainingHashST<Integer, Integer> allSinksSinks = hashTableBuild(graphAllSinksDegrees.sinks());
        assertEquals(3, allSinksSinks.size());
        assertEquals(true, allSinksSinks.contains(1));
        assertEquals(true, allSinksSinks.contains(2));
        assertEquals(true, allSinksSinks.contains(3));
    }

    @Test
    public void testAllSourcesSources() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphAllSourceDegrees.sources());
        assertEquals(3, testHash.size());
        assertEquals(true, testHash.contains(0));
        assertEquals(true, testHash.contains(1));
        assertEquals(true, testHash.contains(2));
    }

    @Test
    public void testAllSourcesSinks() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphAllSourceDegrees.sinks());
        assertEquals(1, testHash.size());
        assertEquals(true, testHash.contains(3));
    }

    //These two methods may be broken;
    @Test
    public void testCyclesMapSources() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphCyclesMapDegrees.sources());
        assertEquals(0, testHash.size());
    }

    @Test
    public void testCycleMapSinks() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphCyclesMapDegrees.sinks());
        assertEquals(0, testHash.size());
    }

    @Test
    public void testLinearSources() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphLinearDegrees.sources());
        assertEquals(1, testHash.size());
        assertEquals(true, testHash.contains(0));
    }

    @Test
    public void testLinearSinks() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphLinearDegrees.sinks());
        assertEquals(1, testHash.size());
        assertEquals(true, testHash.contains(2));
    }

    @Test
    public void testMapSelfLoopSources() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphMapSelfLoopDegrees.sources());
        assertEquals(1, testHash.size());
        assertEquals(true, testHash.contains(0));
    }

    @Test
    public void testMapSelfLoopSinks() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphMapSelfLoopDegrees.sinks());
        assertEquals(0, testHash.size());
    }

    @Test
    public void testMultipleSourcesSources() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphMultipleSourcesDegrees.sources());
        assertEquals(2, testHash.size());
        assertEquals(true, testHash.contains(0));
        assertEquals(true, testHash.contains(4));
    }

    @Test
    public void testMultipleSourcesSinks() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphMultipleSourcesDegrees.sinks());
        assertEquals(3, testHash.size());
        assertEquals(true, testHash.contains(1));
        assertEquals(true, testHash.contains(2));
        assertEquals(true, testHash.contains(3));
    }

    @Test
    public void testNoSourcesSources() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphNoSourcesDegrees.sources());
        assertEquals(0, testHash.size());
    }

    @Test
    public void testNoSourcesSinks() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphNoSourcesDegrees.sinks());
        assertEquals(1, testHash.size());
        assertEquals(true, testHash.contains(2));
    }

    @Test
    public void testTwoSelfLoopsSources() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphTwoSelfLoopsDegrees.sources());
        assertEquals(0, testHash.size());
    }

    @Test
    public void testTwoSelfLoopsSinks() {
        SeparateChainingHashST<Integer, Integer> testHash = hashTableBuild(graphTwoSelfLoopsDegrees.sinks());
        assertEquals(0, testHash.size());
    }

    /*****************************************************************************************************************
     * Testing for sinks and sources
     *****************************************************************************************************************/
    @Test
    public void testAllSinksIsMap() {
        assertEquals(false, graphAllSinksDegrees.isMap());
    }


    @Test
    public void testAllSourcesIsMap() {
        assertEquals(false, graphAllSourceDegrees.isMap());
    }


    @Test
    public void testCyclesMapIsMap() {
        assertEquals(true, graphCyclesMapDegrees.isMap());
    }


    @Test
    public void testLinearIsMap() {
        assertEquals(false, graphAllSourceDegrees.isMap());
    }


    @Test
    public void testMapSelfLoopIsMap() {
        assertEquals(true, graphMapSelfLoopDegrees.isMap());
    }

    @Test
    public void testMultipleSourcesIsMap() {
        assertEquals(false, graphMultipleSourcesDegrees.isMap());
    }


    @Test
    public void testNoSourcesIaMap() {
        assertEquals(false, graphNoSourcesDegrees.isMap());
    }


    @Test
    public void testTwoSelfLoopsIsMap() {
        assertEquals(false, graphTwoSelfLoopsDegrees.isMap());
    }

}