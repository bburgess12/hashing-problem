import org.jgraph.graph.DefaultEdge;
import org.jgrapht.DirectedGraph;
import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.graph.AbstractBaseGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.EdgeReversedGraph;
import org.jgrapht.graph.UnmodifiableDirectedGraph;

import java.io.File;
import java.util.Stack;

/**
 * @author Jonathan Cyr
 *
 * This is an implementation of exercise 4.2.7.
 * The library I used is called JGraphT, and
 * can be found here: http://jgrapht.org/
 *
 */
public class Degrees {

    private DirectedGraph G;
    private int[] inDegree;             // index is vertex, inDegree[v] is indegree of vertex
    private int[] outDegree;            // index is vertex, outDegree[v] is outDegree of vertex
    private Stack<Integer> sources;     // iterable of all sources
    private Stack<Integer> sinks;       // iterable of all sinks
    private boolean isMap;

    /**
     * Given a DirectedGraph g, the constructor computes
     * the indegree and outdegree of every vertex, finds
     * all sources and sinks, and finds out whether the
     * given graph is a map or not.
     * @param g the given DirectedGraph
     */
    public Degrees(DirectedGraph g) {
        G = new UnmodifiableDirectedGraph(g);
        inDegree = new int[G.vertexSet().size()];
        outDegree = new int[G.vertexSet().size()];
        sources = new Stack<Integer>();
        sinks = new Stack<Integer>();
        DirectedGraph rG = new EdgeReversedGraph(g);    // reversed graph to find indegree
        isMap = true;

        // iterate through all vertices to find inDegree,
        // outDegree, sources, and sinks
        for (int v = 0; v < g.vertexSet().size(); v++) {
            inDegree[v] =  g.inDegreeOf(v);
            outDegree[v] = g.outDegreeOf(v);

            if (inDegree[v] == 0) {
                sources.push(v);
            }

            if (outDegree[v] == 0) {
                sinks.push(v);
            }
        }

        // iterate through all vertices to find whether
        // g is a map or not
        for (int v = 0; v < g.vertexSet().size(); v++) {
            if (g.outDegreeOf(v) != 1) {
                isMap = false;
            }
        }

    }

    /**
     * Indegree of vertex v
     * @param v the given vertex
     * @return indegree of v
     */
    public int indegree(int v) {
        return inDegree[v];
    }

    /**
     * Outdegree of vertex v
     * @param v the given vertex
     * @return outdegree of v
     */
    public int outdegree(int v) {
        return outDegree[v];
    }

    /**
     * Iterable of all sources in graph
     * @return stack of all sources
     */
    public Iterable<Integer> sources() {
        return sources;
    }

    /**
     * Iterable of all sinks in graph
     * @return stack of all sinks
     */
    public Iterable<Integer> sinks() {
        return sinks;
    }

    /**
     * Is the graph a map?
     * @return true if graph is a map, false otherwise
     */
    public boolean isMap() {
        return isMap;
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        int V = in.readInt();
        if (V < 0) throw new IllegalArgumentException("Number of vertices must be nonnegative");
        int E = in.readInt();
        if (E < 0) throw new IllegalArgumentException("Number of edges must be nonnegative");
        DirectedGraph<Integer, DefaultEdge> g = new DefaultDirectedGraph<Integer, DefaultEdge>(DefaultEdge.class);

        for (int i = 0; i < V; i++) {
            g.addVertex(i);
        }

        for (int i = 0; i < E; i++) {
            int v = in.readInt();
            int w = in.readInt();
            g.addEdge(v, w);
        }

        Degrees d = new Degrees(g);

        for (int i = 0; i < V; i++) {
            System.out.println("Indegree of " + i + ": " + d.indegree(i));
            System.out.println("Outdegree of " + i + ": " + d.outdegree(i));
            System.out.println();
        }

        System.out.println("Sinks of given graph");
        for (int i : d.sinks()) {
            System.out.println(i);
        }

        System.out.println();

        System.out.println("Sources of given graph");
        for (int i : d.sources()) {
            System.out.println(i);
        }

        System.out.println();

        System.out.println("Is the given graph a map?");
        System.out.println(d.isMap());
    }
}