/**
 * Point class used to represent a point on a graph with the coordinates
 * of X and Y.
 * @author Group A
 *
 */
public class Point implements Comparable<Point> {
  private final int xcoord; // x coordinate
  private final int ycoord; // y coordinate

  /**
   * Initialize the Point class with the given parameters.
   * @param first x coordinate of the point
   * @param last y coordinate of the point
   */
  public Point(int first, int last) {
    xcoord = first;
    ycoord = last;
  }

  /**
   *  Draws this point onto the display using the StdDraw class.
   */
  public void draw() {
    StdDraw.point(xcoord, ycoord);
  }

  /**
   * Draws a line between this point and that point onto the display.
   * @param that The point to draw to.
   */
  public void drawTo(Point that) {
    StdDraw.line(this.xcoord, this.ycoord, that.xcoord, that.ycoord);
  }

  /**
   * Slope between this point and that point.
   * @param that The point to find the slopw to.
   * @return the slope of this point and that point.
   */
  public double slopeTo(Point that) {
    double slope = that.ycoord - this.ycoord;
    slope = slope / (that.xcoord - this.xcoord);
    return slope;

  }

  /**
   * Comparing y-coordinates of this point and that point. Ties are broken by 
   * their x-coordinates.
   * @param that The point to compare to.
   * @return A positive value if this point is > that point,
   *         Negative value is this point is < that point,
   *         0 if this point is equal to that point.
   */
  public int compareTo(Point that) {
    if (this.ycoord > that.ycoord) { // greater than
      return 1;
    }
    if (this.ycoord == that.ycoord) { // breaking tie
      if (this.xcoord > that.xcoord) { // greater than
        return 1;
      } else if (this.xcoord == that.xcoord) { // equal to
        return 0;
      }
    }
    return -1; // less than
  }

  /**
   * Returns the hash code of a point.
   * @return hash code of this point.
   */
  public int hashCode() {
    int hash = 17;
    hash = 31 * hash + ycoord;
    hash = 31 * hash + xcoord;
    return hash;
  }

  /**
   *  Returns the string representation of this point.
   *  @return String representation of this point.
   */
  public String toString() {
    return "(" + xcoord + ", " + ycoord + ")";
  }
}
