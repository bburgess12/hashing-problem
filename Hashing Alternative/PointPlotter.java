import java.util.HashMap;

/*************************************************************************
 *  Compilation:  javac PointPlotter.java
 *  Execution:    java PointPlotter input.txt
 *  Dependencies: Point.java, In.java, StdDraw.java
 *  
 * <p>Given a CSV file in the format: first name, last name, other data,
 * This class constructs a hash table from the following data with Point
 * representing the Key, the first and last name as the Value in a String[].
 * Each point's X-coordinate is the first name's length and similarly for the
 * Y-coordinate with last name.
 * 
 * @author Group A
 *
 *************************************************************************/

public class PointPlotter {
  private static HashMap<Point, Stack<String[]>> hashTable;

  /**
   * Reads in data from the CSV file and constructs a hash table.
   * @param filename The CSV file to be read.
   */
  private static void readInitialize(String filename) {
    if (filename == null) {
      throw new IllegalArgumentException();
    } // exception

    In in = new In(filename);
    Stack<String[]> nameList; // Keeps track of the name list of a point
    String[] name = new String[2]; // [0] = first name; [1] = last name
    hashTable = new HashMap<Point, Stack<String[]>>();

    // Iterate through all lines of the CSV file.
    while (!in.isEmpty()) {
      String[] parse = new String[3]; // Parses each line
      String line = in.readLine(); // Format: {first, last, years}

      parse = line.split(",");
      name[0] = parse[0]; // first name
      name[1] = parse[1]; // last name

      Point point = new Point(name[0].length(), name[1].length());

      // Checks if this Point is already established in the hash table.
      // If so, get the previous list of names. Otherwise, create a new list.
      if (hashTable.containsKey(point)) {
        nameList = hashTable.get(point);
      } else {
        nameList = new Stack<String[]>(); // new name
      }

      boolean alreadyAdded = false;
      // Check if name is already in nameList
      if (!nameList.isEmpty()) { // Only if nameList is not empty
        Stack<String[]> temp = new Stack<String[]>();
        int size = nameList.size();
        temp = nameList; // Backup of nameList to preserve the name order
        for (int current = 0; current < size; current++) {
          String[] nameCheck = nameList.pop(); // check each name in the list
          if (nameCheck.equals(name)) { // Name is already in the list
            alreadyAdded = true; // No need to add the name again
            break;
          }
        }
        nameList = temp;
      } // end of name check.
      // if name is already in the list, move onto next iteration else..
      if (!alreadyAdded) {
        nameList.push(name); // adds the name onto the stack
        hashTable.put(point, nameList); // put into hash table
      }
      name = new String[2]; // clear out previous name
    }
  }

  /**
   * Testing client.
   * @param args name of the CSV file
   */
  public static void main(String[] args) {

    // rescale coordinates and turn on animation mode
    StdDraw.setXscale(0, 35);
    StdDraw.setYscale(0, 35);
    StdDraw.setPenRadius(0.009);
    StdDraw.show(0);

    readInitialize(args[0]);
    
    Iterable<Point> keys = hashTable.keySet();
    for (Point temp : keys) {
      temp.draw();
    }
    // display to screen all at once
    StdDraw.show(0);
  }

}