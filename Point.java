import java.awt.Font;

public class Point implements Comparable<Point> {

	// compare points by slope
	public final int x; // x coordinate
	public final int y; // y coordinate

	// constructor!!
	public Point(int first, int last) {
		x = first;
		y = last;
	}

	// plot this point to standard drawing
	
	public void draw2(int xL, int yL) {
		//StdDraw.setCanvasSize(1000, 1000);
		StdDraw.setXscale(0, xL);
		StdDraw.setYscale(0, yL);
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.setPenRadius(.02);
		StdDraw.line(0, 0, 0, yL);
		StdDraw.line(0, 0, xL, 0);
		StdDraw.show(0);
		StdDraw.setPenRadius(0.002);

		for (int i = xL / 10; i < xL; i = i + xL / 10) {
			StdDraw.setXscale(0, xL);
			StdDraw.setYscale(0, yL);
			StdDraw.setPenColor(StdDraw.GRAY);
			StdDraw.line(i, 0, i, yL);
			StdDraw.show(0);
		}
		for (int i = yL / 10; i < yL; i = i + yL / 10) {
			//StdDraw.setCanvasSize(1000, 1000);
			StdDraw.setXscale(0, xL);
			StdDraw.setYscale(0, yL);
			StdDraw.setPenColor(StdDraw.GRAY);
			StdDraw.line(0, i, xL, i);
			StdDraw.show(0);
		}

	}

	public void draw(int xL, int yL) {
		//StdDraw.setCanvasSize(1000, 1000);
		StdDraw.setXscale(0, xL);
		StdDraw.setYscale(0, yL);
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.filledCircle(x, y, .05);
		StdDraw.show(0);

	}
	public void draw3(int xL, int yL, int x, int y, Stack<String> name) {
		int line = 0;
		
		while (!name.isEmpty()) {
			StdDraw.setFont(new Font("TimesRoman", Font.HANGING_BASELINE, 20));
			StdDraw.setPenColor(StdDraw.BLUE);
			//StdOut.println(name.size());
			StdDraw.text(x + line, y + line, name.pop() + " , " + name.pop(), 20);
			line = line + 200;
			
		}

	}


	// draw line between this point and that point to standard drawing
	public void drawTo(Point that) {
		StdDraw.line(this.x, this.y, that.x, that.y);
	}

	// slope between this point and that point
	public double slopeTo(Point that) {
		double s = that.y - this.y;
		s = s / (that.x - this.x);
		return s;

	}

	// comparing y-coordinates and breaking ties by x-coordinates
	public int compareTo(Point that) {
		if (this.y > that.y) {
			return 1;
		} else if (this.y == that.y) {
			if (this.x > that.x) {
				return 1;
			} else if (this.x == that.x) {
				return 0;
			} else {
				return -1;
			}
		} else {
			return -1;
		}
	}

	// return string representation of this point
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}